﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp13
{
    public enum Mast { Risti, Ruutu, Ärtu, Poti}
    public enum Tugevus { Kaks=2, Kolm, Neli, Viis, Kuus, Seitse, Kaheksa, Üheksa, Kümme, Soldat, Emand, Kuningas, Äss}

    
    public struct Kaart
    {
        public Mast mast;
        public Tugevus tugevus;

        public override string ToString()
        {
            return $"{mast} {tugevus}";
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            // hakatuseks teeme mängijate massiivi
            string[] mängijad = { "Elmar", "Sille", "Willi", "Norbert" };

            // nüüd teeme kaardipaki nagu ta tuleb tehasest (nr-d 0..51)
            List<int> algseis = new List<int>();
            for (int i = 0; i < 52; i++)
            {
                algseis.Add(i);
            }

            Random r = new Random(); // see meie sõber juhulike arvude meister

            // teeme uue listi, kuhu tõmmame juhulikke kaarte
            List<int> segatud = new List<int>();

            while (algseis.Count > 0) // seni kuni on veel kaarte mida võtta
            {
                // järjekorranumber, milline võtta
                int i = r.Next() % algseis.Count;
                segatud.Add(algseis[i]); // paneme segatud pakki
                algseis.RemoveAt(i); // kustutame tehasepakist ära
            }

            // jagame kaardid nelja kuhja (igale mängijale 13 kaarti)
            int[,] jagatud = new int[4, 13];
            for (int i = 0; i < 52; i++)
              //jagatud[i / 13, i % 13] = segatud[i];  // jagamine igale 13 kaarti
                jagatud[i % 4, i % 4] = segatud[i];    // jagamine kordamööda
            // Palun selet aenda jaoks lahti see!!!


            // trükime välja kellel mis kaardid on
            for (int i = 0; i < 4; i++) // mängijate kaupa
            {
                Console.WriteLine($"Mängijal {mängijad[i]} on kaardid: ");
                for (int j = 0; j < 13; j++)  // mängija kaardid
                {
                    int k = jagatud[i, j];   // järgmise kaardi number
                                             // mast = k / 13
                                             // tugevus = k % 13
                    Kaart kaart = new Kaart { mast = (Mast)(k / 13), tugevus = (Tugevus)(k % 13 + 2) };
                    Console.Write($"{kaart} ");
                }
                Console.WriteLine();  // uuele reale uus mängia
            }
        }
    }
}
